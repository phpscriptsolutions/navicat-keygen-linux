# Navicat Keygen - Cách build？
[README](how-to-build.md)

## 1. Điều kiện yêu cầu


   1. Phải đảm bảo GCC của bạn hỗ trợ tính năng C++17.
   ```console
   # Check C++17
   $ gcc -v --help 2> /dev/null | sed -n '/^ *-std=\([^<][^ ]\+\).*/ {s//\1/p}'
   ...
   f2003
   f2008
   f95
   gnu
   legacy
   c++0x
   c++98
   c++17
   c89
   c99
   c9x
   gnu++0x
   gnu++98
   gnu89
   gnu99
   gnu9x
   iso9899:1990
   iso9899:199409
   iso9899:1999
   iso9899:199x
   ...

   ``` 

   Nếu dùng ubuntu 18.04 thì cần update version của GCC
   ```console
   # Check vesion
   $ gcc --version
   Output:
      gcc (Ubuntu 7.4.0-1ubuntu1~18.04) 7.4.0
      Copyright (C) 2017 Free Software Foundation, Inc.
      This is free software; see the source for copying conditions.  There is NO
      warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   
   # Nâng cấp GCC và dùng nhiều version khác nhau
   
   $ sudo apt install software-properties-common
   $ sudo add-apt-repository ppa:ubuntu-toolchain-r/test
   $ sudo apt install gcc-7 g++-7 gcc-8 g++-8 gcc-9 g++-9
   
   # Thêm các version của GCC vào
   $ sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 90 --slave /usr/bin/g++ g++ /usr/bin/g++-9 --slave /usr/bin/gcov gcov /usr/bin/gcov-9
   $ sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 80 --slave /usr/bin/g++ g++ /usr/bin/g++-8 --slave /usr/bin/gcov gcov /usr/bin/gcov-8
   $ sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 70 --slave /usr/bin/g++ g++ /usr/bin/g++-7 --slave /usr/bin/gcov gcov /usr/bin/gcov-7
   
   # Dùng lệnh sau để chuyển đổi các version
   $ sudo update-alternatives --config gcc
   
   OUTPUT:
   There are 3 choices for the alternative gcc (providing /usr/bin/gcc).

   Selection    Path            Priority   Status
   ------------------------------------------------------------
   * 0            /usr/bin/gcc-9   90        auto mode
     1            /usr/bin/gcc-7   70        manual mode
     2            /usr/bin/gcc-8   80        manual mode
     3            /usr/bin/gcc-9   90        manual mode
   
   Press <enter> to keep the current choice[*], or type selection number: 0
   
   Hoặc:
   
   $ sudo apt-get install software-properties-common
   $ sudo add-apt-repository ppa:jonathonf/gcc-9.0
   $ sudo apt-get update
   $ sudo apt-get install gcc-9

   ``` 
   2. Hãy đảm bảo rằng bạn đã cài đặt các thư viện sau：

   * `build-essential`
   * `libssl-dev`
   * `capstone`
   * `keystone`
   * `rapidjson`


      Nếu bạn sử dụng Ubuntu, bạn có thể cài đặt chúng bằng cách：
   
      ```console
      # install build-essential
      $ sudo apt-get install build-essential
      
      # install libssl-dev
      $ sudo apt-get install libssl-dev
      
      # install capstone
      $ sudo apt-get install libcapstone-dev
   
      # install keystone
      $ sudo apt-get install cmake
      $ git clone https://github.com/keystone-engine/keystone.git
      $ cd keystone
      $ mkdir build
      $ cd build
      $ ../make-share.sh
      $ sudo make install
      $ sudo ldconfig
   
      # install rapidjson
      $ sudo apt-get install rapidjson-dev
      ```


## 2. Build

```console
$ git clone -b linux --single-branch https://gitlab.com/phpscriptsolutions/navicat-keygen-linux.git
$ cd navicat-keygen-linux
$ make all
```

Bạn sẽ thấy các tệp thực thi trong thư mục `bin /`.
